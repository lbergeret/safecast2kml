#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Copyright (C) 2012  Lionel Bergeret
#
# ----------------------------------------------------------------
# The contents of this file are distributed under the CC0 license.
# See http://creativecommons.org/publicdomain/zero/1.0/
# ----------------------------------------------------------------

# system libraries
import os, sys, time, traceback, operator
from optparse import OptionParser
import numpy as np
import csv
import shutil

# from https://code.google.com/p/regionator/
# tutorial: http://www.google.fr/earth/outreach/tutorials/region.html
import kml.csvregionator

# Japan limits
JP_lat_min = 32.0
JP_lon_min = 130.0
JP_lat_max = 46.00
JP_lon_max = 147.45

KMLIconColors = ["white", "midgreen", "green", "lightGreen", "yellow",
                 "orange", "darkOrange", "red", "darkRed", "grey"]
KMLIconBins = [0, 35, 70, 105, 175, 280, 350, 420, 680, 1050]

def processMeasurements(filename, geofilename, enableWorld):
  print "Processing %s ..." % filename

  # Load data
  data = csv.reader(open(filename))

  # Read the column names from the first line of the file
  fields = data.next()

  safecast = csv.writer(open(geofilename, 'wb'), delimiter='|')

  # Process data
  processedLines = 0
  errorLines = 0
  for row in data:
    # Zip together the field names and values
    items = zip(fields, row)

    # Add the value to our dictionary
    item = {}
    for (name, value) in items:
       item[name] = value.strip()

    try:
      # Ignore if outside limits
      if not ((float(item["Latitude"])>JP_lat_min) and (float(item["Latitude"])<JP_lat_max) and (float(item["Longitude"])>JP_lon_min) and (float(item["Longitude"])<JP_lon_max)) and not enableWorld:
        continue
    except:
      print "ERROR:",row
      errorLines+=1
      continue

    if (item["Unit"] == "cpm"):
      processedLines+=1
      score = np.digitize(np.array([int(item["Value"])]), KMLIconBins)[0]
      safecast.writerow([score, item["Latitude"], item["Longitude"], "%s CPM" % item["Value"], item["Captured Time"], "safecastStyle.kml#%s" %  KMLIconColors[score-1]])

  print "Total data loaded = %d (%d errors)" % (processedLines, errorLines)


progressed = 0
def callbackPlacemark(p):
  global progressed
  progressed += 1
  if progressed%100000 == 0:
    print "%d data points loaded." % progressed

def csvRegionator(filename):
  print "Processing %s ..." % filename
  codec = 'UTF-8'
  min_lod = 256
  max_per = 64*4 # max points per region
  rootkml = "safecast.kml"
  dir = "kmls"
  global_styleUrl = None #"safecastStyle.kml"
  verbose = False #True

  # Create output folder
  if os.path.exists(dir):
    shutil.rmtree(dir)
  os.makedirs(dir)
  shutil.copy2("safecastStyle.kml", dir)

  # Generate RbNL KML
  kml.csvregionator.RegionateCSV(filename, codec, min_lod, max_per,
                                 rootkml, dir, verbose, global_styleUrl, callback = callbackPlacemark)
  print "Done."

# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
  parser = OptionParser("Usage: geocsv [options] <log-file>")
  parser.add_option("-w", "--world",
                      action="store_true", dest="world", default=False,
                      help="disable Japan region filter")

  (options, args) = parser.parse_args()

  if len(args) != 1:
      parser.error("Wrong number of arguments")

  if not os.path.exists('safecast.csv'):
    processMeasurements(args[0], 'safecast.csv', options.world)

  csvRegionator('safecast.csv')

  print "zip -r -9 safecast.kmz safecast.kml kmls/"
